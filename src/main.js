import Vue from 'vue'
import App from './App.vue'
import * as Sentry from "@sentry/browser"
import { Integrations } from "@sentry/tracing"

Sentry.init({
  Vue,
  dsn: "https://f4cb8ba381ae44c88056ba1dafaf51d6@o488500.ingest.sentry.io/5548991",
  integrations: [
    new Integrations.BrowserTracing(),
  ],

  // We recommend adjusting this value in production, or using tracesSampler
  // for finer control
  tracesSampleRate: 1.0,
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
